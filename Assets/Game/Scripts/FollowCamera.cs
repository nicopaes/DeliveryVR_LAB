using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform mainVRCamera;

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.localPosition = mainVRCamera.localPosition;
        //this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, mainVRCamera.localRotation, 0.8f);
        this.transform.localRotation = mainVRCamera.localRotation;
    }
}
