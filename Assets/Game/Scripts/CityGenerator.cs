using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class CityGenerator : SerializedMonoBehaviour
{
    public Dictionary<int, GameObject> intToPrefab = new Dictionary<int, GameObject>();
    public int maximumBlocks = 50;
    int currentBlocks = 0;

    [OdinSerialize]//, OnValueChanged("GenerateCity")]
    public int[,] CityBase = new int[10, 10]; 

    [Button]
    public void RandomizeCity()
    {
        currentBlocks = 0; 
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if(currentBlocks < maximumBlocks)
                { 
                    CityBase[i, j] = Random.Range(0, intToPrefab.Count + 1);
                    if(CityBase[i, j] > 0)
                    {
                        currentBlocks++;
                    }
                }
            }
        }
    }

    [Button]
    public void GenerateCity()
    {
        currentBlocks = 0;
        GameObject parent = new GameObject("GeneratedCity");
        parent.transform.position = new Vector3(-6 * 15 * 10, 0, -6 * 15 * 10);
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (intToPrefab.ContainsKey(CityBase[i, j]) && currentBlocks < maximumBlocks)
                {
                    Vector3 pos = new Vector3(i * 6 * 20, 0, j * 6 * 20);
                    GameObject blockCreated = Instantiate(intToPrefab[CityBase[i, j]], Vector3.zero, Quaternion.identity);
                    blockCreated.transform.SetParent(parent.transform);
                    blockCreated.transform.localPosition = pos;
                    currentBlocks++;
                }                
            }
        }
    }

}
