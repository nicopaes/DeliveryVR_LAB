using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.SceneManagement;

public class DeliveryManager : Singleton<DeliveryManager>
{
    public int sucessfullDeliveries = 0;

    [SerializeReference]
    public TargetArea currentTargetArea = null;
    public Action<TargetArea> OnTargetAreaEnter;
    public List<TargetArea> PossibleTargetAreas = new List<TargetArea>();
    public TMP_Text currentDeliveries;

    private void Start()
    {
        PossibleTargetAreas.Clear();
        PossibleTargetAreas = GameObject.FindObjectsOfType<TargetArea>(true).ToList();
        sucessfullDeliveries = 0;
        OnTargetAreaEnter += (area) => DeliverPackage(area);
        //
        currentTargetArea = PossibleTargetAreas[0];
        currentTargetArea.Initialization();
    }

    public void Update()
    {
        currentDeliveries.text = sucessfullDeliveries.ToString();
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    [Button]
    public void PingCurrentDeliveryArea()
    {
        currentTargetArea.PingArea();
    }

    public void DeliverPackage(TargetArea area)
    {
        area.gameObject.SetActive(false);
        var listWithoutLast = PossibleTargetAreas.Where(x => x != area).ToList();
        int randomInt = UnityEngine.Random.Range(0, listWithoutLast.Count());
        currentTargetArea = listWithoutLast[randomInt];
        currentTargetArea.Initialization();
    }

    public void RandomDeliveryArea()
    {
        currentTargetArea.gameObject.SetActive(false);
        var listWithoutLast = PossibleTargetAreas.Where(x => x != currentTargetArea).ToList();
        int randomInt = UnityEngine.Random.Range(0, listWithoutLast.Count());
        currentTargetArea = listWithoutLast[randomInt];
        currentTargetArea.Initialization();
    }
}
