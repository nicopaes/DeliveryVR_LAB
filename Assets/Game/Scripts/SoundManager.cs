using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;

public class SoundManager : Singleton<SoundManager>
{
    [OdinSerialize]
    public Dictionary<string, AudioClip> dictionaryAudioClips = new Dictionary<string, AudioClip>();

    public Dictionary<string, AudioSource> dictionaryAudioSource = new Dictionary<string, AudioSource>();

    private IEnumerator Start() 
    {
        yield return new WaitForSeconds(1f);
        PlaySound("CityAmbience_2");
    }

    [Button]
    public void PlaySound(string key)
    {
        if(dictionaryAudioSource.ContainsKey(key)) dictionaryAudioSource[key].Play();
    }

    [Button]
    public void StopSound(string key)
    {
        if(dictionaryAudioSource.ContainsKey(key))
        {
            if(dictionaryAudioSource[key].isPlaying)
            {
                dictionaryAudioSource[key].Stop();
            }
        }
    }

    public void SetVolume(string key, float value)
    {
        if (dictionaryAudioSource.ContainsKey(key))
        {
            dictionaryAudioSource[key].volume = value;
        }
    }

    [Button]
    public void UpdateAudioSources()
    {
        foreach (KeyValuePair<string,AudioClip> kvp in dictionaryAudioClips)
        {
            if(GameObject.Find(kvp.Key + "_AudioSource") == null)
            {
                GameObject newObject = new GameObject(kvp.Key + "_AudioSource");
                newObject.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

                newObject.name = kvp.Key + "_AudioSource";
                newObject.transform.SetParent(this.transform);
                //
                AudioSource newAudioSource = newObject.AddComponent<AudioSource>();
                newAudioSource.clip = kvp.Value;
                dictionaryAudioSource.Add(kvp.Key, newAudioSource);
                newAudioSource.playOnAwake = false;
                newAudioSource.volume = 0.2f;
            }
        }  
    }
}
