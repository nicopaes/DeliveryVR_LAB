using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class TargetArea : MonoBehaviour
{
    [ColorUsage(hdr: true, showAlpha: true)]
    public Color activeColor;
    [ColorUsage(hdr: true, showAlpha: true)]
    public Color inactiveColor;
    private Material _materialInstance;
    private GameObject pingCilinder;

    private void Awake()
    {
        _materialInstance = GetComponent<Renderer>().materials[0];
        pingCilinder = transform.GetChild(0).gameObject;
    }

    public void Initialization()
    {
        this.gameObject.SetActive(true);
        if (_materialInstance == null)
        {
            _materialInstance = GetComponent<Renderer>().materials[0];
        }
        pingCilinder.SetActive(false);
        ChangeColor(activeColor);
    }

    public void PingArea()
    {
        StartCoroutine(PingAreaCor());
        IEnumerator PingAreaCor()
        {
            pingCilinder.SetActive(true);
            yield return new WaitForSecondsRealtime(3f);
            pingCilinder.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ship"))
        {
            ChangeColor(inactiveColor);
            DeliveryManager.Current.OnTargetAreaEnter.Invoke(this);
            Debug.Log("Ship Entered AREA!");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ship"))
        {
            ChangeColor(activeColor);
            Debug.Log("Ship Entered EXIT!");
        }
    }

    void ChangeColor(Color newColor)
    {
        _materialInstance.SetColor("_MainColor", newColor);
    }
}
