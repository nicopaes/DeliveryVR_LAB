using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HurricaneVR.Framework.Core.Player;
using TMPro;
using HurricaneVR.Framework.Components;
using Sirenix.OdinInspector;

public class ShipController : MonoBehaviour
{
    [BoxGroup("Status Engine"), ReadOnly]
    public bool engineIsRunning = false;
    [BoxGroup("Status Engine")]
    public Material engineOnMaterial;
    [BoxGroup("Status Engine")]
    public Material engineOffMaterial;
    [BoxGroup("Status Engine")]
    public MeshRenderer engineIndicatorRenderer;

    [BoxGroup("Steering Status"), ReadOnly]
    public float frontPower = 0f;
    [BoxGroup("Steering Status")]
    public float frontPowerMultiplier = 2f;
    [BoxGroup("Steering Status")]
    public HVRRotationTracker steeringRotationTracker;
    [BoxGroup("Steering Status")]
    public HVRPhysicsDial steeringPhysicsDial;
    [BoxGroup("Steering Status"), ReadOnly]
    public float steeringAngle;
    [BoxGroup("Steering Status")]
    public HVRRotationTracker upAndDownRotationTracker;
    [BoxGroup("Steering Status")]
    public float upAngle = 90f;
    [BoxGroup("Steering Status")]
    public float upPower = 0f;
    [BoxGroup("Steering Status")]
    public float upPowerMultiplier = 2f;

    [BoxGroup("Second engine")]
    public bool secondEngineActive = false;
    [BoxGroup("Second engine")]
    public float secondFrontPower = 0;
    [BoxGroup("Second engine")]
    public float frontSecondPowerMultiplier = 3f;

    [BoxGroup("Velocity Viz")]
    public LineRenderer lineRenderer;
    [BoxGroup("Velocity Viz")]
    public LineRenderer lineRenderer2;
    [BoxGroup("Velocity Viz")]
    public Vector3 offset;

    [BoxGroup("Reposition Button")]
    public bool canUseRepositionButtons = false;


    [BoxGroup("Overheating")]
    public float overheating = 0f;
    [BoxGroup("Overheating")]
    public bool overheatButtonStatus => overheatButton.IsPressed;
    [BoxGroup("Overheating")]
    public HVRPhysicsButton overheatButton;


    [Header("Status")]
    public float mass = 10f;
    public float maximumSteeringForce = 10f;
    public Vector3 rotationOffset = Vector3.zero;
    public Vector3 targetoffset = new Vector3(1, 0, 0);
    public Vector3 currentVelocity = Vector3.zero;
    public float currentVelocityMagnitude = 0f;
    public float maximumAceleration = 4f;
    public float maximumVelocity = 4f;

    public HVRPlayerController playerCharacterController;
    public Transform shipBody;
    public Transform targetTransform;
    public Transform targetTransformViz;
    public Transform steeringWheel;

    public Vector3 fixedPlayerLocalPosition;

    [BoxGroup("UI")]
    [BoxGroup("UI/Velocity")]
    public TMP_Text currentVelocityTextX;
    [BoxGroup("UI/Velocity")]
    public TMP_Text currentVelocityTextY;
    [BoxGroup("UI/Velocity")]
    public TMP_Text currentVelocityTextZ;
    [BoxGroup("UI/Alt")]
    public TMP_Text currentAltitudeText;
    [BoxGroup("UI/Over")]
    public TMP_Text currentOverHeatText;

    private void Awake()
    {
        engineIsRunning = false;
        frontPower = 0f;
        upAngle = 90f;
        overheating = 0f;
    }

    void GetInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            targetTransform.localPosition += Vector3.right * 0.01f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            targetTransform.localPosition -= Vector3.right * 0.01f;
        }
        if (targetTransform.localPosition.magnitude > 0.5f)
        {
            if (Input.GetKey(KeyCode.A))
            {
                targetTransform.localPosition += Vector3.forward * 0.01f;
            }
            if (Input.GetKey(KeyCode.D))
            {
                targetTransform.localPosition -= Vector3.forward * 0.01f;
            }
        }
        if (Input.GetKey(KeyCode.Space))
        {
            targetTransform.localPosition += Vector3.up * 0.01f;
        }
        if (Input.GetKey(KeyCode.C))
        {
            targetTransform.localPosition -= Vector3.up * 0.01f;
        }
    }
    // Update is called once per frame
    void Update()
    {
        upAngle = upAndDownRotationTracker.Angle;
        steeringAngle = steeringRotationTracker.Angle;
        if (Mathf.Abs(steeringAngle) < 0.5f)
        {
            steeringAngle = 0.0f;
        }
        if (steeringAngle > 540f)
        {
            steeringPhysicsDial.Joint.targetAngularVelocity = Vector3.zero;
            steeringPhysicsDial.TargetAngularVelocity = 0f;
            steeringRotationTracker.transform.localRotation = Quaternion.Euler(539f, 0f, 0f);
        }
        else if (steeringAngle < -540f)
        {
            steeringPhysicsDial.Joint.targetAngularVelocity = Vector3.zero;
            steeringPhysicsDial.TargetAngularVelocity = 0f;
            steeringRotationTracker.transform.localRotation = Quaternion.Euler(-539f, 0f, 0f);
        }

        if (engineIsRunning)
        { 
            targetTransform.localPosition = (Vector3.right * frontPowerMultiplier * frontPower/180f);
            targetTransform.localPosition += (Vector3.up * upPowerMultiplier * upAngle/90f);
            if (targetTransform.localPosition.magnitude > 0.5f)
            {
                targetTransform.localPosition += (-Vector3.forward * maximumAceleration * steeringAngle/540f);
            }
            if(secondFrontPower > 0f)
            {
                secondEngineActive = true;
                targetTransform.localPosition += (Vector3.right * frontSecondPowerMultiplier * secondFrontPower/180f);
                maximumAceleration = 40f;
                maximumVelocity = 40f;
                maximumSteeringForce = 30f;
            }
            else
            {
                secondEngineActive = false;
                maximumVelocity = 20f;
                maximumAceleration = 20f;
                maximumSteeringForce = 15f;
            }
            
        }
        else
        {
            targetTransform.localPosition = Vector3.zero;
        }
        ///
        targetTransformViz.localPosition = targetTransform.localPosition * 10f;
        if (targetTransform.localPosition.x < 0f && targetTransform.localPosition.magnitude < 0.1f)
        {
            Debug.Log("Zera X");
            targetTransform.localPosition = new Vector3(0, 0, targetTransform.localPosition.z);
        }
        else if (targetTransform.localPosition.x > maximumAceleration)
        {
            Debug.Log("Cap X");
            targetTransform.localPosition = new Vector3(maximumAceleration, targetTransform.localPosition.y, targetTransform.localPosition.z);
        }
        //
        if (targetTransform.localPosition.z > targetTransform.localPosition.x)
        {
            targetTransform.localPosition = new Vector3(targetTransform.localPosition.x, targetTransform.localPosition.y, targetTransform.localPosition.x);
        }
        else if (targetTransform.localPosition.z < -targetTransform.localPosition.x)
        {
            targetTransform.localPosition = new Vector3(targetTransform.localPosition.x, targetTransform.localPosition.y, -targetTransform.localPosition.x);
        }        

        Vector3 _desireVelocity =
        (targetTransform.position - this.transform.position).normalized
        * Vector3.Distance(targetTransform.position, this.transform.position);
        ///      

        if (!overheatButton.IsPressed)
        {
            if (currentVelocity.magnitude >= maximumVelocity * 0.45f)
            {
                overheating += Time.deltaTime * currentVelocity.magnitude / 10f;
            }
            else if (overheating > 0f)
            {
                overheating -= Time.deltaTime * currentVelocity.magnitude / 10f;
            }
        }
        else
        {
            overheating -= 1f * Time.deltaTime;
        }
        //
        if (overheating > 100f)
        {
            _desireVelocity /= 10f;
        }

        Vector3 _steering = _desireVelocity - currentVelocity;

        _steering = Vector3.ClampMagnitude(_steering, maximumSteeringForce);
        _steering /= (mass); //TODO URGENT This is necessary because right now the only way to keep enemies from pushing lena minimizing the mass. But this messes up the smooth movement, so this is a hack

        currentVelocity = Vector3.ClampMagnitude(currentVelocity + _steering, maximumVelocity);

        

        if (true)
        {
            Debug.DrawRay(this.transform.position, _desireVelocity * 100f, Color.cyan);
            Debug.DrawRay(this.transform.position, _steering * 100f, Color.magenta);
            Debug.DrawRay(this.transform.position, currentVelocity * 100f, Color.red);
            Debug.DrawLine(this.transform.position, targetTransform.position, Color.white);
        }

        lineRenderer.SetPosition(0, this.transform.position);
        lineRenderer.SetPosition(1, this.transform.position + currentVelocity * 25f);
        //
        lineRenderer2.SetPosition(0, this.transform.position);
        lineRenderer2.SetPosition(1, this.transform.position + _desireVelocity * 25f);

        transform.position += currentVelocity * Time.deltaTime;
        Vector3 zxVelocity = new Vector3(currentVelocity.x, 0, currentVelocity.z);
        if (currentVelocity.magnitude > 0.2f)
        {
            if(zxVelocity.magnitude > 0.1f)
            {
                shipBody.rotation = Quaternion.LookRotation(zxVelocity, Vector3.up) * Quaternion.Euler(rotationOffset);
            }
            else
            {
                shipBody.rotation = Quaternion.LookRotation(new Vector3(1f, 0, 0f), Vector3.up) * Quaternion.Euler(rotationOffset);
            }          
        }
        else
        {
            steeringWheel.localRotation = Quaternion.Euler(rotationOffset);
        }
        UpdateUI();
        UpdateSound();
    }

    private void LateUpdate()
    {
        currentVelocityMagnitude = currentVelocity.magnitude;
    }

    [BoxGroup("Status Engine"), Button]
    public void ToggleEnginePower()
    {
        if(!engineIsRunning)
        {            
            StartCoroutine(engineStartup());
        }
        else
        {
            engineIndicatorRenderer.material = engineOffMaterial;
            engineIsRunning = false;
        }

        IEnumerator engineStartup()
        {
            SoundManager.Current.PlaySound("EngineStartup");
            yield return new WaitForSecondsRealtime(6.9f);
            engineIsRunning = true;
            engineIndicatorRenderer.material = engineOnMaterial;
            SoundManager.Current.PlaySound("SmallExplosion");
            SoundManager.Current.PlaySound("EngineIdle");
            SoundManager.Current.PlaySound("EngineIdle_Alt");
            SoundManager.Current.PlaySound("EngineIdle_Large");
            yield return new WaitForSecondsRealtime(0.5f);
            SoundManager.Current.PlaySound("EngineIdle_2");
        }
    }

    public void SetRightLeverAngle(Single value, Single value2)
    {
        frontPower = value;
    }

    public void SetSecondEngineAngle(Single value, Single value2)
    {
        secondFrontPower = value;
    }

    public void SetUpDownLeverAngle(Single value, Single value2)
    {
        upPower = value;
    }

    public void UpdateUI()
    {
        //currentVelocityText.text = currentVelocity.ToString();
        //currentVelocityMagnitudeText.text = $"{currentVelocity.magnitude.ToString()} ({maximumAceleration})";
        //currentTargetText.text = targetTransform.localPosition.ToString();
        //overheatingText.text = overheating.ToString();
        currentVelocityTextX.text = currentVelocity.x.ToString("0.0");
        currentVelocityTextY.text = currentVelocity.y.ToString("0.0");
        currentVelocityTextZ.text = currentVelocity.z.ToString("0.0");
        currentAltitudeText.text = this.transform.position.y.ToString("0.0");
        currentOverHeatText.text = overheating.ToString("0.0");
    }

    public void UpdateSound()
    {
        SoundManager.Current.SetVolume("EngineIdle", 0.7f + frontPower/900f); //frontPower/180f / 5f 180*5f = 900f
        SoundManager.Current.SetVolume("EngineIdle_Large", secondFrontPower/900f);
    }
}
