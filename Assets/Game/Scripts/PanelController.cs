using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PanelController : MonoBehaviour
{
    public PanelModes currentMode;

    public List<GameObject> PanelModeListGO = new List<GameObject>();

    public void Awake()
    {
        currentMode = PanelModes.Mode1;
    }
       
    [Button]
    public void ChangeMode()
    {
        if(currentMode == PanelModes.Mode1)
        {
            PanelModeListGO[0].SetActive(false);
            PanelModeListGO[1].SetActive(true);
            PanelModeListGO[2].SetActive(false);
            PanelModeListGO[3].SetActive(false);
            currentMode = PanelModes.Mode2;
        }
        else if(currentMode == PanelModes.Mode2)
        {
            PanelModeListGO[0].SetActive(false);
            PanelModeListGO[1].SetActive(false);
            PanelModeListGO[2].SetActive(true);
            PanelModeListGO[3].SetActive(false);
            currentMode = PanelModes.Mode3;
        }
        else if (currentMode == PanelModes.Mode3)
        {
            PanelModeListGO[0].SetActive(true);
            PanelModeListGO[1].SetActive(false);
            PanelModeListGO[2].SetActive(false);
            PanelModeListGO[3].SetActive(false);
            currentMode = PanelModes.Mode4;
        }
        else if (currentMode == PanelModes.Mode4)
        {
            PanelModeListGO[0].SetActive(false);
            PanelModeListGO[1].SetActive(false);
            PanelModeListGO[2].SetActive(false);
            PanelModeListGO[3].SetActive(true);
            currentMode = PanelModes.Mode1;
        }
    }

    public enum PanelModes
    {
        Mode1,
        Mode2,
        Mode3,
        Mode4
    }
}


